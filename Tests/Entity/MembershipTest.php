<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Tests\Entity;

use Chill\GroupBundle\Entity\Membership;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MembershipTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var \Prophecy\Prophet 
     */
    protected $prophet;
    
    
    public function setUp()
    {
        $this->prophet = new \Prophecy\Prophet;
    }
    
    public function tearDown()
    {
        $this->prophet->checkPredictions();
    }
    
    public function testValidRoleForGroupInvalid()
    {
        $context = $this->prophet->prophesize();
        $violation = $this->prophet->prophesize();
        $violation->willImplement(\Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface::class);
        $violation->atPath('role')->willReturn($violation->reveal());
        $violation->addViolation()->willReturn(null);
        
        $context->willImplement(\Symfony\Component\Validator\Context\ExecutionContextInterface::class);
        $context->buildViolation('This role is not allowed by the group type')
                ->willReturn($violation->reveal());
        
        // as we will use invalid data, the method "buildViolation" should be called
        $context->buildViolation('This role is not allowed by the group type')
                ->shouldBeCalled();
        
        $role1 = (new \Chill\GroupBundle\Entity\Role)
                ->setName(array('fr' => 'role1'))
                ->setActive(true);
        $role2 = (new \Chill\GroupBundle\Entity\Role)
                ->setName(array('fr' => 'role2'))
                ->setActive(true);
        
        $type = (new \Chill\GroupBundle\Entity\Type())
                ->addRole($role1);
        
        $group = (new \Chill\GroupBundle\Entity\CGroup)
                ->setType($type);
        
        $membership = (new Membership)
                ->setCgroup($group)
                ->setRole($role2);
        
        $membership->validRoleForGroup($context->reveal());
        
    }
    
    public function testValidRoleForGroupValid()
    {
        $context = $this->prophet->prophesize();
        $violation = $this->prophet->prophesize();
        $violation->willImplement(\Symfony\Component\Validator\Violation\ConstraintViolationBuilderInterface::class);
        $violation->atPath('role')->willReturn($violation->reveal());
        $violation->addViolation()->willReturn(null);
        
        $context->willImplement(\Symfony\Component\Validator\Context\ExecutionContextInterface::class);
        $context->buildViolation('This role is not allowed by the group type')
                ->willReturn($violation->reveal());
        
        // as we will use invalid data, the method "buildViolation" should **not** be called
        $context->buildViolation('This role is not allowed by the group type')
                ->shouldNotBeCalled();
        
        $role1 = (new \Chill\GroupBundle\Entity\Role)
                ->setName(array('fr' => 'role1'))
                ->setActive(true);
        
        $type = (new \Chill\GroupBundle\Entity\Type())
                ->addRole($role1);
        
        $group = (new \Chill\GroupBundle\Entity\CGroup)
                ->setType($type);
        
        $membership = (new Membership)
                ->setCgroup($group)
                ->setRole($role1);
        
        $membership->validRoleForGroup($context->reveal());
        
    }
}
