<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Chill\MainBundle\Entity\Center;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MembershipControllerTest extends WebTestCase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    protected $client;
    
    /**
     * The center associated with the current entity
     *
     * @var \Chill\MainBundle\Entity\Center
     */
    protected $center;
    
    public function setUp()
    {
        self::bootKernel();
        
        $this->em = self::$kernel->getContainer()
                ->get('doctrine.orm.entity_manager')
                ;
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'center a_social',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
        
        $this->center = $this->em->getRepository('ChillMainBundle:Center')
                ->findOneBy(array('name' => 'Center A'));
    }
    
    /**
     * get a random person belonging to a given center
     * 
     * @param Center $center
     * @return \Chill\PersonBundle\Entity\Person
     */
    protected function getRandomPerson(Center $center)
    {
        $personIds = $this->em->createQuery('SELECT p.id '
                . 'FROM ChillPersonBundle:Person p '
                . 'WHERE p.center = :center')
                ->setParameter('center', $center)
                ->getScalarResult();
        
        $id = $personIds[array_rand($personIds)]['id'];

        return $this->em->getRepository('ChillPersonBundle:Person')
                ->find($id);
    }
    
    /**
     * 
     * @param Center $center
     * @return \Chill\GroupBundle\Entity\CGroup
     */
    protected function getRandomCGroup(Center $center)
    {
        $groupIds = $this->em->createQuery('SELECT g.id '
                . 'FROM ChillGroupBundle:CGroup g '
                . 'WHERE g.center = :center')
                ->setParameter('center', $center)
                ->getScalarResult();
        
        $id = $groupIds[array_rand($groupIds)]['id'];
        
        return $this->em->getRepository('ChillGroupBundle:CGroup')
                ->find($id);
    }
    
    /**
     * 
     * @return \Chill\GroupBundle\Entity\Type
     */
    protected function getRandomType()
    {
        $types = $this->em->getRepository('ChillGroupBundle:Type')
                ->findAll();
        
        return $types[array_rand($types)];
    }
    
    
    
    public function testListByPerson()
    {
        $personId = $this->getRandomPerson($this->center)->getId();

        $this->client->request('GET', '/fr/group/membership/by_person/'.$personId);
        
        $this->assertTrue($this->client->getResponse()->isSuccessFul());
        
    }
    
    public function testAddingAMembershipWithExistingGroup()
    {
        $person = $this->getRandomPerson($this->center);
        // select a group where person is not member of
        do {
            $group = $this->getRandomCGroup($this->center);
        } while ( in_array($group, $this->em->getRepository('ChillGroupBundle:CGroup')
                ->findGroupsAttachedTo($person)) );
        
        // count memberships of this person
        $nbMemberships = $this->em->createQuery('SELECT count(m.id) '
                . 'FROM ChillGroupBundle:Membership m '
                . 'WHERE m.person = :person')
                ->setParameter('person', $person)
                ->getSingleScalarResult();
        
        $crawler = $this->client->request('GET', '/fr/group/membership/new', array(
            'person_id' => $person->getId()));
        
        $this->assertTrue($this->client->getResponse()->isSuccessFul());
        
        // get the form
        $formCrawler = $crawler->filter('form[name=membership]');
        $this->assertGreaterThan(0, $formCrawler->count());
        
        $form = $formCrawler->selectButton('Envoi')
                ->form(
                array(
                    'membership[cgroup]' => $group->getId(),
                    'membership[role]' => $group->getType()->getRoles()->first()->getId()
                ));
        
        $crawler = $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect());
        
        $crawler = $this->client->followRedirect();
        
        $this->assertGreaterThan(0, $crawler
                ->filter('html:contains("'.$group->getName().'")')->count(),
                "the new group appears on the page");
        $this->assertEquals($nbMemberships + 1, $this->em->createQuery('SELECT count(m.id) '
                . 'FROM ChillGroupBundle:Membership m '
                . 'WHERE m.person = :person')
                ->setParameter('person', $person)
                ->getSingleScalarResult(),
                "Test the number of memberships as increased");
        
    }
    
    public function testAddingAMembershipWithNewGroup()
    {
        $person = $this->getRandomPerson($this->center);
        $type = $this->getRandomType();
        $groupName = uniqid('new group in test ');
        // count memberships of this person
        $nbMemberships = $this->em->createQuery('SELECT count(m.id) '
                . 'FROM ChillGroupBundle:Membership m '
                . 'WHERE m.person = :person')
                ->setParameter('person', $person)
                ->getSingleScalarResult();
        
        $crawler = $this->client->request('GET', '/fr/group/membership/new', array(
            'person_id' => $person->getId()));
        
        $this->assertTrue($this->client->getResponse()->isSuccessFul());
        
        // get the form
        $formCrawler = $crawler->filter('form[name=membership_with_group]');
        $this->assertGreaterThan(0, $formCrawler->count());
        
        $form = $formCrawler->selectButton('Envoi')
                ->form(
                array(
                    'membership_with_group[cgroup][name]' => $groupName,
                    'membership_with_group[cgroup][type]' => $type->getId(),
                    'membership_with_group[role]' => $type->getRoles()->first()->getId()
                ));
        
        $crawler = $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect());
        
        $crawler = $this->client->followRedirect();
        
        $this->assertGreaterThan(0, $crawler
                ->filter('html:contains("'.$groupName.'")')->count(),
                "the new group appears on the page");
        $this->assertEquals($nbMemberships + 1, $this->em->createQuery('SELECT count(m.id) '
                . 'FROM ChillGroupBundle:Membership m '
                . 'WHERE m.person = :person')
                ->setParameter('person', $person)
                ->getSingleScalarResult(),
                "Test the number of memberships as increased");
        
    }
    
    public function testUpdateMembership()
    {
        // we find a person who is associated with memberships and a group type which
        // has more than 1 role (to allow test switching
        do {
            $person = $this->getRandomPerson($this->center);
            $memberships = $this->em->getRepository('ChillGroupBundle:Membership')
                    ->findByPerson($person);
            if (count($memberships) > 0) {
                /* @var $membership \Chill\GroupBundle\Entity\Membership */
                $membership = $memberships[0];
                $group = $membership->getCgroup();
                $groupType = $group->getType();
                $role = $membership->getRole();
            }
        } while (!(isset($membership) AND count($groupType->getRoles()) > 1));
        
        $crawler = $this->client->request('GET', sprintf(
                '/fr/group/membership/%d/edit', $membership->getId()));
        
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(),
                "test that the status code of the edit page is 200");
        
        $button = $crawler->selectButton('Modifier');
        
        $this->assertEquals(1, $button->count(), "a button is present on the page");
        
        $form = $button->form();
        
        do {
            /* @var $variable \Chill\GroupBundle\Entity\Role */
            $anotherRole = $groupType->getRoles()
                    ->get(array_rand($groupType->getRoles()->getKeys()));
        } while ($anotherRole->getId() === $role->getId());
        
        $this->client->submit($form, array(
            'membership[role]' => $anotherRole->getId()
        ));
        
        $this->assertTrue($this->client->getResponse()
                ->isRedirect('/fr/group/membership/by_person/'.$person->getId()),
                "the response of submitting the form is a redirection");
        
        $this->em->refresh($membership);
        
        $this->assertEquals($anotherRole->getId(), $membership->getRole()->getId(),
                "test that the role membership has changed");
    }
    
    public function testRemoveMembership()
    {
        // we find a person who is associated with memberships and a group type which
        // has more than 1 role (to allow test switching
        do {
            $person = $this->getRandomPerson($this->center);
            $memberships = $this->em->getRepository('ChillGroupBundle:Membership')
                    ->findByPerson($person);
            if (count($memberships) > 0) {
                /* @var $membership \Chill\GroupBundle\Entity\Membership */
                $membership = $memberships[0];
                $person = $membership->getPerson();
            }
        } while (!isset($membership));
        
        $crawler = $this->client->request('GET', sprintf('/fr/group/membership/%d/delete', 
                $membership->getId()));
        
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(),
                "test that the status code of the edit page is 200");
        
        $button = $crawler->selectButton('Confirmer');
        
        $this->assertEquals(1, $button->count(), "a button 'confirmer' is present");
        
        $form = $button->form();
        
        $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()
                ->isRedirect('/fr/group/membership/by_person/'.$person->getId()),
                "the response of submitting the form is a redirection");
        
        $this->assertEquals(
                0, 
                $this->em->createQuery(
                            'SELECT COUNT(m.id) '
                            . 'FROM ChillGroupBundle:Membership m '
                            . 'WHERE m.id = :membership_id'
                        )
                    ->setParameter('membership_id', $membership->getId())
                    ->getSingleScalarResult(),
                "assert that the membership is not present any more in database");
    }
    
}
