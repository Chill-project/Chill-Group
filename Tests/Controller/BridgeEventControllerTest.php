<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BridgeEventControllerTest extends WebTestCase
{
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $em;
    
    /**
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    protected $client;
    
    /**
     * The center associated with the current entity
     *
     * @var \Chill\MainBundle\Entity\Center
     */
    protected $center;
    
    public function setUp()
    {
        self::bootKernel();
        
        $this->em = self::$kernel->getContainer()
                ->get('doctrine.orm.entity_manager')
                ;
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'center a_social',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
        
        $this->center = $this->em->getRepository('ChillMainBundle:Center')
                ->findOneBy(array('name' => 'Center A'));
    }
    
    /**
     * Test the feature "add a participation from group".
     * 
     * The group must not be empty.
     */
    public function testAddParticipation()
    {
        // get a group with more than 1 members
        $group_ids = $this->em->createQuery('SELECT g.id, COUNT(m.id) '
                . 'FROM ChillGroupBundle:CGroup g '
                    . 'JOIN ChillGroupBundle:Membership m WITH g.id = m.cgroup '
                    . 'JOIN ChillMainBundle:Center c WITH c.id = g.center '
                . 'WHERE c = :center '
                . 'GROUP BY g.id '
                . 'HAVING COUNT(m.id) > 1 ')
                ->setParameter('center', $this->center->getId())
                ->getResult(); 
        
        if (count($group_ids) === 0) {
            throw new \RuntimeException("The database does not contains groups "
                    . "with > 1 members and belonging to center A");
        }
        
        $group = $this->em->getRepository('ChillGroupBundle:CGroup')
                ->find($group_ids[array_rand($group_ids)]['id']);
        
        $c = $this->client->request('GET', '/fr/group/event/new_participation_for_group', array(
            'cgroup_id' => $group->getId(), 'event_id' => rand(0,10)
        )); 
        
        $this->assertTrue($this->client->getResponse()->isRedirect(), 
                "Test the answer is a redirection");
        
        // test the redirection is ok (parameters are ok)
        $this->client->followRedirect();
        
        $this->assertContains('/fr/event/participation/new', 
                $this->client->getHistory()->current()->getUri());
        
        // test the people are present somewhere in the URL
        $expected_ids = $group->getMembers()
                ->map(function ($m) { return $m->getPerson()->getId(); });
        
        foreach($expected_ids as $id) {
            $this->assertContains("$id", $this->client->getHistory()->current()->getUri(),
                    "Test that the uri contains the id $id");
        }
    }
    
}
