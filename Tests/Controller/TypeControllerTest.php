<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Link;

class TypeControllerTest extends WebTestCase
{
    /**
     * an authenticated client, created durint setUp
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    private $client;
    
    public function setUp()
    {
        self::bootKernel();
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'admin',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
    }
    
    public function testList()
    {
        $crawler = $this->client->request('GET', '/fr/admin/group/type/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 
                "Unexpected HTTP status code for GET /fr/admin/group/type/");
        $link = $crawler->selectLink('Créer un nouveau type de groupe')->link();
        
        $this->assertInstanceOf(\Symfony\Component\DomCrawler\Link::class, $link);
        $this->assertRegExp('|/fr/admin/group/type/new$|', $link->getUri());
    }
    
    public function testNew()
    {
        $crawler = $this->client->request('GET', '/fr/admin/group/type/new');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 
                "Unexpected HTTP status code for GET /admin/group/type/");
        
        $name = uniqid('test group ');
        
        $form = $crawler->selectButton('Créer')->form(array(
            'chill_groupbundle_type[name][fr]' => $name
        ));
        
        $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect(),
                "failing that the form submit is redirecting to show page");
        
        $crawler = $this->client->followRedirect();
        
        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("'.$name.'")')->count(), 
                'Missing text "'.$name.'"');
        
        $update = $crawler->selectLink('Modifier')->link();
        
        $this->assertInstanceOf(Link::class, $update);
        $this->assertRegExp('|/fr/admin/group/type/[0-9]{1,}/edit$|', $update->getUri());
        
        return $update;
    }
    
    /**
     * @depends testNew
     */
    public function testUpdate(Link $link)
    {
        $crawler = $this->client->request('GET', $link->getUri());
        
        $name = uniqid('test group updated ');
        $form = $crawler->selectButton('Mettre à jour')->form(array(
            'chill_groupbundle_type[name][fr]' => $name
        ));
        
        $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect(),
                "failing that the form submit is redirecting");
        $this->client->followRedirect();
        
        $this->assertRegExp('|/fr/admin/group/type/[0-9]{1,}/edit$|', 
                $this->client->getHistory()->current()->getUri(),
                "check that we are redirected on the edit page");
        
        $crawler = $this->client->request('GET', '/fr/admin/group/type/');
        
        $this->assertGreaterThan(0, $crawler->filter('td:contains("'.$name.'")')->count(),
                "check that the list contains the updated name");
        
    }

}
