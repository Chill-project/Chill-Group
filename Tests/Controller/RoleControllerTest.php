<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Chill\GroupBundle\Entity\Type;
use Symfony\Component\DomCrawler\Link;

class RoleControllerTest extends WebTestCase
{
    /**
     *
     * @var Type
     */
    protected static $type;


    public static function setUpBeforeClass()
    {
        self::bootKernel();
        
        $groupType = new Type();
        $groupType->setActive(true)
                ->setName(array('fr' => 'Type for role controller test _ '.uniqid()))
                ;
        
        /* @var $em \Doctrine\ORM\EntityManagerInterface */
        $em = static::$kernel->getContainer()
                ->get('doctrine.orm.entity_manager');
        $em->persist($groupType);
        $em->flush();
        
        self::$type = $groupType;
    }
    
    /**
     * an authenticated client, created durint setUp
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    private $client;
    
    public function setUp()
    {
        self::bootKernel();
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'admin',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
    }
    
    public function testNew()
    {
        $crawler = $this->client->request('GET', '/fr/admin/group/role/new', 
                array('type_id' => self::$type->getId()));
        
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        
        $name = uniqid('test role ');
        
        $form = $crawler->selectButton('Créer')->form(array(
            'chill_groupbundle_role[name][fr]' => $name
        ));
        
        $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect(),
                "failing that the form submit is redirecting to show page");
        
        $crawler = $this->client->followRedirect();
        
        // Check data in the show view
        $this->assertGreaterThan(0, $crawler->filter('td:contains("'.$name.'")')->count(), 
                'Missing text "'.$name.'"');
        
        $update = $crawler->selectLink('Modifier')->link();
        
        $this->assertInstanceOf(Link::class, $update);
        $this->assertRegExp('|/fr/admin/group/role/[0-9]{1,}/edit$|', $update->getUri());
        
        return $update;
    }
    
    /**
     * @depends testNew
     */
    public function testUpdate(Link $link)
    {
        $crawler = $this->client->request('GET', $link->getUri());
        
        $name = uniqid('test role updated ');
        $form = $crawler->selectButton('Mettre à jour')->form(array(
            'chill_groupbundle_role[name][fr]' => $name
        ));
        
        $this->client->submit($form);
        
        $this->assertTrue($this->client->getResponse()->isRedirect(),
                "failing that the form submit is redirecting");
        $this->client->followRedirect();
        
        $this->assertRegExp('|/fr/admin/group/role/[0-9]{1,}/edit$|', 
                $this->client->getHistory()->current()->getUri(),
                "check that we are redirected on the edit page");
        
        $crawler = $this->client->request('GET', '/fr/admin/group/type/'
                .self::$type->getId().'/show');
        
        $this->assertTrue($this->client->getResponse()->isSuccessful());
        $this->assertGreaterThan(0, $crawler->filter('td:contains("'.$name.'")')->count(),
                "check that the list contains the updated name");
        
    }
}
