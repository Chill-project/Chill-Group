<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add chill_group table to database
 */
class Version20160212165636 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 
                'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_group_cgroup_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_group_membership_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_group_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_group_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_group_cgroup ('
                . 'id INT NOT NULL, '
                . 'type_id INT DEFAULT NULL, '
                . 'name VARCHAR(255) NOT NULL, '
                . 'active BOOLEAN NOT NULL, '
                . 'center_id INT DEFAULT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A89320C1C54C8C93 ON chill_group_cgroup (type_id)');
        $this->addSql('CREATE INDEX IDX_A89320C15932F377 ON chill_group_cgroup (center_id)');
        $this->addSql('CREATE TABLE chill_group_membership ('
                . 'id INT NOT NULL, '
                . 'person_id INT DEFAULT NULL, '
                . 'role_id INT DEFAULT NULL, '
                . 'cgroup_id INT DEFAULT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_388DD2D217BBB47 ON chill_group_membership (person_id)');
        $this->addSql('CREATE INDEX IDX_388DD2DD60322AC ON chill_group_membership (role_id)');
        $this->addSql('CREATE INDEX IDX_388DD2D1891AFE1 ON chill_group_membership (cgroup_id)');
        $this->addSql('CREATE TABLE chill_group_role ('
                . 'id INT NOT NULL, name JSON NOT NULL, '
                . 'active BOOLEAN NOT NULL, '
                . 'type_id INT DEFAULT NULL,'
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_group_type ('
                . 'id INT NOT NULL, name JSON NOT NULL, '
                . 'active BOOLEAN NOT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE chill_group_cgroup '
                . 'ADD CONSTRAINT FK_A89320C1C54C8C93 FOREIGN KEY (type_id) '
                . 'REFERENCES chill_group_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_group_membership '
                . 'ADD CONSTRAINT FK_388DD2D217BBB47 FOREIGN KEY (person_id) '
                . 'REFERENCES Person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_group_membership '
                . 'ADD CONSTRAINT FK_388DD2DD60322AC FOREIGN KEY (role_id) '
                . 'REFERENCES chill_group_role (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_group_membership '
                . 'ADD CONSTRAINT FK_388DD2D1891AFE1 FOREIGN KEY (cgroup_id) '
                . 'REFERENCES chill_group_cgroup (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_group_role '
                . 'ADD CONSTRAINT FK_9CE0598FC54C8C93 FOREIGN KEY (type_id) '
                . 'REFERENCES chill_group_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_9CE0598FC54C8C93 ON chill_group_role (type_id)');
        $this->addSql('ALTER TABLE chill_group_cgroup '
                . 'ADD CONSTRAINT FK_A89320C15932F377 FOREIGN KEY (center_id) '
                . 'REFERENCES centers (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 
                'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_group_membership DROP CONSTRAINT FK_388DD2D1891AFE1');
        $this->addSql('ALTER TABLE chill_group_membership DROP CONSTRAINT FK_388DD2DD60322AC');
        $this->addSql('ALTER TABLE chill_group_cgroup DROP CONSTRAINT FK_A89320C1C54C8C93');
        $this->addSql('ALTER TABLE chill_group_cgroup DROP CONSTRAINT FK_A89320C15932F377');
        $this->addSql('DROP SEQUENCE chill_group_cgroup_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_group_membership_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_group_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_group_type_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_group_cgroup');
        $this->addSql('DROP TABLE chill_group_membership');
        $this->addSql('DROP TABLE chill_group_role');
        $this->addSql('DROP TABLE chill_group_type');
    }
}
