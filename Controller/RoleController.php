<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, <http://www.champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Chill\GroupBundle\Entity\Role;
use Chill\GroupBundle\Form\RoleType;

/**
 * Role controller.
 * 
 * **note** : there is no index action: the 
 *
 */
class RoleController extends Controller
{

    /**
     * Creates a new Role entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Role();
        $type_id = $request->query->getInt('type_id', null);
        
        if ($type_id !== NULL) {
            $type = $this->getDoctrine()->getManager()
                    ->getRepository('ChillGroupBundle:Type')
                    ->find($type_id);
            
            if ($type === NULL) {
                throw $this->createNotFoundException('The type is not found');
            }
            
            $entity->setType($type);
        }
        
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $this->addFlash('success', $this->get('translator')->trans(
                    'The role is created'));
            
            return $this->redirect($this->generateUrl('admin_group_role_show', array('id' => $entity->getId())));
        } else {
            $this->addFlash('error', $this->get('translator')->trans(
                    'The provided role is invalid'));
        }

        return $this->render('ChillGroupBundle:Role:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Role $entity)
    {
        $action = $entity->getType() === NULL ? 
            $this->generateUrl('admin_group_role_create') :
            $this->generateUrl('admin_group_role_create', array(
                'type_id' => $entity->getType()->getId()
            ));
        
        $form = $this->createForm(RoleType::class, $entity, array(
            'action' => $action,
            'method' => 'POST',
        ));

        // the type is selected by the controller
        $form->remove('type');
        
        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Role entity.
     *
     */
    public function newAction(Request $request)
    {
        $entity = new Role();
        $type_id = $request->query->getInt('type_id', null);
        
        if ($type_id !== NULL) {
            $type = $this->getDoctrine()->getManager()
                    ->getRepository('ChillGroupBundle:Type')
                    ->find($type_id);
            
            if ($type === NULL) {
                throw $this->createNotFoundException('The type is not found');
            }
            
            $entity->setType($type);
        }
        
        
        $form   = $this->createCreateForm($entity);

        return $this->render('ChillGroupBundle:Role:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Role entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillGroupBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        return $this->render('ChillGroupBundle:Role:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing Role entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillGroupBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('ChillGroupBundle:Role:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Role entity.
    *
    * @param Role $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Role $entity)
    {
        $form = $this->createForm(RoleType::class, $entity, array(
            'action' => $this->generateUrl('admin_group_role_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    
    /**
     * Edits an existing Role entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillGroupBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')->trans(
                    'The role is updated'));
            
            return $this->redirect($this->generateUrl('admin_group_role_edit', array('id' => $id)));
        } else {
            $this->addFlash('error', $this->get('translator')->trans(
                    'The provided role is invalid'));
        }

        return $this->render('ChillGroupBundle:Role:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
}
