<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\GroupBundle\Entity\Membership;
use Chill\GroupBundle\Form\MembershipType;
use Chill\GroupBundle\Form\CGroupType;
use Chill\GroupBundle\Entity\CGroup;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Controller for person membership
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MembershipController extends Controller
{
    
    /**
     * List membership for a person
     * 
     * @param int $person_id
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException if not granted
     */
    public function listByPersonAction($person_id)
    {
        $em = $this->getDoctrine()->getManager();
        $person = $em->getRepository('ChillPersonBundle:Person')
                ->find($person_id);
        
        if ($person === NULL) {
            throw $this->createNotFoundException('Person not found');
        }
        
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $person);
        
        //$this->denyAccessUnlessGranted('CHILL_GROUP_SEE', $person);
        
        $memberships = $em->getRepository('ChillGroupBundle:Membership')
                ->findBy(array('person' => $person));
        
        return $this->render('ChillGroupBundle:Membership:list.html.twig', array(
            'person' => $person,
            'memberships' => $memberships
        ));
    }
    
    public function newAction(Request $request)
    {
        $membership = new Membership();
        $parameters = $this->handleRequest($request);
        
        $this->handleMembership($parameters, $membership);
        
        // check for ACL
        if ($membership->getPerson() !== NULL) {
                        
            $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        }
        
        $form = $this->createCreateForm($membership, $parameters);
        $formWithGroup = $this->createCreateGroupForm($parameters);
        
        return $this->render('ChillGroupBundle:Membership:new.html.twig', array(
            'form' => $form->createView(),
            'form_with_group' =>$formWithGroup->createView(),
            'membership' => $membership,
            'person' => $membership->getPerson()
        ));
    }
    
    public function editAction($membership_id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $membership = $em->getRepository('ChillGroupBundle:Membership')
                ->find($membership_id);
        
        if ($membership === NULL) {
            throw $this->createNotFoundException('membership not found');
        }
        
        // check for ACL
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        
        
        $form = $this->createEditForm($membership);
        
        return $this->render('ChillGroupBundle:Membership:edit.html.twig', array(
            'form' => $form->createView(),
            'membership' => $membership,
            'person' => $membership->getPerson()
        ));
    }
    
    public function updateAction(Request $request, $membership_id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $membership = $em->getRepository('ChillGroupBundle:Membership')
                ->find($membership_id);
        
        if ($membership === NULL) {
            throw $this->createNotFoundException('membership not found');
        }
        
        // check for ACL
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        
        $form = $this->createEditForm($membership);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em->flush();
                
                $this->addFlash('success', $this->get('translator')
                        ->trans("The membership was updated."));
                return $this->redirectToRoute('chill_group_membership_by_person', [
                    'person_id' => $membership->getPerson()->getId()
                ]);
            }
        }
        
        return $this->render('ChillGroupBundle:Membership:edit.html.twig', array(
            'form' => $form->createView(),
            'membership' => $membership,
            'person' => $membership->getPerson()
        ));
    }
    
    public function createAction(Request $request)
    {
        $membership = new Membership();
        $parameters = $this->handleRequest($request);
        
        $this->handleMembership($parameters, $membership);
        
        if ($membership->getPerson() !== NULL) {
            
            $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        }
        
        $form = $this->createCreateForm($membership, $parameters);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $membership = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($membership);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans('The membership is created'));
            
            return $this->redirectToRoute('chill_group_membership_by_person', 
                    array('person_id' => $membership->getPerson()->getId()));
        } 
        
        return $this->render('ChillGroupBundle:Membership:new.html.twig', array(
            'form' => $form->createView(),
            'membership' => $membership,
            'person' => $membership->getPerson()
        ));
    }
    
    public function deleteAction(Request $request, $membership_id)
    {
        $em = $this->getDoctrine()->getManager();
        
        /* @var $membership Membership */
        $membership = $em->getRepository('ChillGroupBundle:Membership')
                ->find($membership_id);
        
        if ($membership === NULL) {
            throw $this->createNotFoundException('membership not found');
        }
        
        // check for ACL
        $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        
        $form = $this->createDeleteForm($membership);
        
        if ($request->getMethod() === Request::METHOD_POST) {
            $form->handleRequest($request);
            
            if ($form->isSubmitted() && $form->isValid()) {
                $em->remove($membership);
                $em->flush();
                
                $this->addFlash('success', $this->get('translator')->trans(
                            'The membership to the group "%group%" was removed.',
                            array(
                                '%group%' => $membership->getCgroup()->getName()
                            )
                        ));
                
                /* @var $logger \Psr\Log\LoggerInterface */
                $logger = $this->get('chill.main.logger');
                $logger->info("A group membership have been removed", array(
                    'group' => $membership->getCgroup()->getName(),
                    'group_id' => $membership->getCgroup()->getId(),
                    'person_lastname' => $membership->getPerson()->getLastName(),
                    'person_firstname' => $membership->getPerson()->getFirstName(),
                    'person_id' => $membership->getPerson()->getId(),
                    'by_user' => $this->getUser()->getUsername()
                ));
                
                return $this->redirectToRoute('chill_group_membership_by_person', array(
                    'person_id' => $membership->getPerson()->getId()
                ));
            }
        }
        
        return $this->render('ChillGroupBundle:Membership:confirm_delete.html.twig', array(
            'form' => $form->createView(),
            'membership' => $membership
        ));
    }
    
    /**
     * 
     * @param Membership $membership
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createDeleteForm(Membership $membership)
    {
        $builder = $this->createFormBuilder(null, array(
            'action' => $this->generateUrl('chill_group_membership_delete', array(
                'membership_id' => $membership->getId()
            )),
            'method' => 'POST'
        ));
        
        $builder->add('submit', SubmitType::class, array(
            'label' => 'Confirm'
        ));
        
        return $builder->getForm();
    }
    
    public function createWithGroupAction(Request $request)
    {
        $cgroup = new CGroup();
        $membership = new Membership();
        $parameters = $this->handleRequest($request);
        
        $this->handleMembership($parameters, $membership);
        
        if ($membership->getPerson() !== NULL) {
            
            $this->denyAccessUnlessGranted('CHILL_PERSON_SEE', $membership->getPerson());
        }
        
        $cgroup->setCenter($membership->getPerson()->getCenter());
        
        $form = $this->createCreateGroupForm($parameters, $cgroup);
        $form->handleRequest($request);
        
        if ($form->isValid()) {
            $data = $form->getData();
            /* @var $group CGroup */
            $group = $data['cgroup'];
            /* @var $role \Chill\GroupBundle\Entity\Role */
            $role = $data['role'];
            
            $em = $this->getDoctrine()->getManager();
            
            $em->persist($group);
            
            // update membership
            $membership->setCgroup($group)
                    ->setRole($role)
                    ;
            $em->persist($membership);
            
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans('The group and the membership were created'));
            
            return $this->redirectToRoute('chill_group_membership_by_person', 
                    array('person_id' => $membership->getPerson()->getId()));
            
        }
        
        foreach($form->getErrors() as $e) {
            var_dump($e->getOrigin()->getName());
        }
        
        return $this->render('ChillGroupBundle:Membership:new.html.twig', array(
            'form_with_group' => $form->createView(),
            'membership' => $membership,
            'person' => $membership->getPerson()
        ));
        
    }
    
    /**
     * Isolate the parameters useful for action. The returned array may 
     * be used to :
     * 
     * - self::createCreateForm, as the argument for $routeParameters
     * - self::handleMembership, to handle membership and configure it with the
     * given parameters
     * 
     * @param Request $request
     * @return array
     */
    protected function handleRequest(Request $request)
    {
        return array(
            'person_id' => $request->query->getInt('person_id', null)
        );
    }
    
    protected function handleMembership($parameters, Membership $membership)
    {
        $em = $this->getDoctrine()->getManager();
        
        if ($parameters['person_id'] !== NULL) {
            $person = $em->getRepository('ChillPersonBundle:Person')
                    ->find($parameters['person_id']);
            if ($person === NULL) {
                throw $this->createNotFoundException('person not found');
            }

            
            $membership->setPerson($person);
        }
    }
    
    /**
     * 
     * @param Membership $membership
     * @param array $routeParameters the parameters to append on the route
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createCreateForm(Membership $membership, array $routeParameters = array())
    {
        $d = $this->get('chill_group.form.membership');
        //var_dump(get_class($d));
        $form = $this->createForm($d, $membership, array(
            'action' => $this->generateUrl('chill_group_membership_create', $routeParameters),
            'method' => 'POST',
            'center' => $membership->getPerson()->getCenter(),
            'person' => $membership->getPerson()
        ));
        
        $form->add('submit', 'submit', array(
            'label' => 'Submit'
        ));
        
        return $form;
    }
    
    /**
     * 
     * @param Membership $membership
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createEditForm(Membership $membership)
    {
        $d = $this->get('chill_group.form.membership');
        //var_dump(get_class($d));
        $form = $this->createForm($d, $membership, array(
            'action' => $this->generateUrl('chill_group_membership_update', array(
                'membership_id' => $membership->getId(),
                'person_id' => $membership->getPerson()->getId()
            )),
            'method' => 'POST',
            'center' => $membership->getPerson()->getCenter(),
            'person' => $membership->getPerson(),
            'status' => 'edit'
        ));
        
        $form->add('submit', 'submit', array(
            'label' => 'Edit'
        ));
        
        return $form;
    }
    
    /**
     * 
     * @param array $routeParameters
     * @param CGroup $group
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createCreateGroupForm(array $routeParameters = array(), CGroup $group = null)
    {
        // create local copy for Closure
        $helper = $this->get('chill.main.helper.translatable_string');
        $roleRepository = $this->get('chill_group.repository.role');
        
        $builder = $this->get('form.factory')->createNamedBuilder(
                'membership_with_group',
                \Symfony\Component\Form\Extension\Core\Type\FormType::class,
                array(
                    'cgroup' => $group === null ? new CGroup() : $group,
                    ),
                array(
                    'action' => $this->generateUrl('chill_group_membership_create_with_group', $routeParameters),
                    'method' => 'POST'
                )
            );
        
        $builder->add('cgroup', CGroupType::class);
        $builder->get('cgroup')->remove('center');
        
        $builder->get('cgroup')
                ->addEventListener(
                        FormEvents::PRE_SET_DATA, 
                        function(FormEvent $event) use ($group, $roleRepository, $helper) {
                    
                            MembershipType::formModifier(
                                    $event->getForm()->getRoot(), 
                                    $roleRepository, 
                                    $helper, 
                                    $group === NULL ? null : $group->getType());
                        });
                       
        $builder->get('cgroup')->get('type')
                ->addEventListener(
                        FormEvents::POST_SUBMIT, 
                        function(FormEvent $event) use ($roleRepository, $helper) {
                            $type = $event->getForm()->getData();
                    
                            MembershipType::formModifier(
                                    $event->getForm()->getRoot(), 
                                    $roleRepository, 
                                    $helper, 
                                    $type);
                        });

        
        $builder->add('submit', 'submit', array(
            'label' => 'Submit'
        ));
        
        return $builder->getForm();
    }
    
}
