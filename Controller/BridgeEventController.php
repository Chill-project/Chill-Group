<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Chill\GroupBundle\Entity\Membership;
use Chill\EventBundle\Entity\Participation;

/**
 * Controller which makes the bridge between the ChillGroupBundle and
 * the ChillEventBundle.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class BridgeEventController extends Controller
{
    /**
     * Method to add participation on an Event to a group.
     * 
     * @param Request $request
     */
    public function addParticipationAction(Request $request) 
    {
        $em = $this->getDoctrine()->getManager();
        $event_id = $request->query->getInt('event_id', null);
        $cgroup_id = $request->query->getInt('cgroup_id', null);
        
        if ($event_id === NULL) {
            throw $this->createNotFoundException('event parameter not given.');
        }
        
        /* @var $cgroup \Chill\GroupBundle\Entity\CGroup */
        $cgroup = $em->getRepository('ChillGroupBundle:CGroup')->find($cgroup_id);
        
        if ($cgroup === NULL) {
            throw $this->createNotFoundException('Group not found');
        }
        
//        $this->denyAccessUnlessGranted('CHILL_GROUP_SEE', $cgroup, 
//                'Access denied : CHILL_GROUP_SEE');
        
        
        $persons_ids = $cgroup->getMembers()->map(
                function (Membership $m) { return $m->getPerson()->getId(); }
                );
                
        if ($persons_ids->count() === 0) {
            $this->addFlash('error', $this->get('translator')->trans(
                    'Nobody may be added on this event: it seems that the group is empty.')
                    );
            
            return $this->redirectToRoute('chill_event__event_show', array(
                'event_id' => $event_id
            ));
        }
            
        return $this->redirectToRoute('chill_event_participation_new', array(
            'event_id' => $event_id,
            'persons_ids' => implode(',', $persons_ids->toArray())
            ));

    }
    
}
