<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Entity;

/**
 * A group of people.
 * 
 * Note : The name "group" is reserved. CGroup is used instead. "C" make a 
 * reference to "Chill"
 * 
 * @internal it seems that we must implement \ArrayAccess to make CGroup accessible
 * from embedded CGroupType (see MembershipController::createCreateGroupForm
 * 
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CGroup implements \Chill\MainBundle\Entity\HasCenterInterface, \ArrayAccess
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $active = true;
    
        /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $members;

    /**
     * @var \Chill\GroupBundle\Entity\Type
     */
    private $type;
    
    /**
     *
     * @var \Chill\MainBundle\Entity\Center
     */
    private $center;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->members = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add member
     *
     * @param \Chill\GroupBundle\Entity\Members $member
     *
     * @return CGroup
     */
    public function addMember(\Chill\GroupBundle\Entity\Membership $member)
    {
        $this->members[] = $member;

        return $this;
    }

    /**
     * Remove member
     *
     * @param \Chill\GroupBundle\Entity\Membership $member
     */
    public function removeMember(\Chill\GroupBundle\Entity\Membership $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Set type
     *
     * @param \Chill\GroupBundle\Entity\Type $type
     *
     * @return CGroup
     */
    public function setType(\Chill\GroupBundle\Entity\Type $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Chill\GroupBundle\Entity\Type
     */
    public function getType()
    {
        return $this->type;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CGroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return CGroup
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Get the center
     * 
     * @return \Chill\MainBundle\Entity\Center
     */
    public function getCenter()
    {
        return $this->center;
    }

    /**
     * Set the center
     * 
     * @param \Chill\MainBundle\Entity\Center $center
     * @return \Chill\GroupBundle\Entity\CGroup
     */
    public function setCenter(\Chill\MainBundle\Entity\Center $center)
    {
        $this->center = $center;
        return $this;
    }

    public function offsetExists($offset)
    {
        return in_array($offset, array('center', 'id', 'name', 'active', 'members', 'type'));
    }

    public function offsetGet($offset)
    {
        switch($offset) {
            case 'center' : return $this->getCenter();
            case 'id' : return $this->getId();
            case 'name' : return $this->getName();
            case 'active' : return $this->getActive();
            case 'members' : return $this->getMembers();
            case 'type': return $this->getType();
        }
    }

    public function offsetSet($offset, $value)
    {
        switch($offset) {
            case 'center': $this->setCenter($value);
                break;
            case 'id': throw new \RuntimeException('you should not add an id to this entity');
            case 'name' : $this->setName($value);
                break;
            case 'active': $this->setActive($value);
                break;
            case 'type': $this->setType($value);
                break;
            case 'members': 
                if ($value instanceof \Doctrine\Common\Collections\Collection) {
                    foreach ($value as $member) {
                        $this->addMember($member);
                    }
                } elseif ($value instanceof Membership) {
                    $this->addMember($value);
                } else {
                    throw new \RuntimeException('the type given for offset "members" is not type '.Membership::class);
                }
                break;
        }
    }

    public function offsetUnset($offset)
    {
        switch($offset) {
            case 'center': $this->setCenter(null);
                break;
            case 'id' : 
                throw new \RuntimeException('you should not reset id of an entity');
                break;
            case 'name':
                $this->setName('');
                break;
            case 'active':
                $this->setActive(true);
                break;
            case 'members':
                $this->members->clear();
                break;
            case 'type':
                $this->setType(null);
                break;
        }
    }

}
