<?php

/*
 * Chill is a software for social workers
 *
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Entity;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Make the link between a group and a person, caracterized by a
 * certain role.
 */
class Membership
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $person;

    /**
     * @var \Chill\GroupBundle\Entity\Role
     */
    private $role;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set person
     *
     * @param \Chill\PersonBundle\Entity\Person $person
     *
     * @return Membership
     */
    public function setPerson(\Chill\PersonBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Chill\PersonBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set role
     *
     * @param \Chill\GroupBundle\Entity\Role $role
     *
     * @return Membership
     */
    public function setRole(\Chill\GroupBundle\Entity\Role $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Chill\GroupBundle\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }
    /**
     * @var \Chill\GroupBundle\Entity\CGroup
     */
    private $cgroup;


    /**
     * Set cgroup
     *
     * @param \Chill\GroupBundle\Entity\CGroup $cgroup
     *
     * @return Membership
     */
    public function setCgroup(\Chill\GroupBundle\Entity\CGroup $cgroup = null)
    {
        $this->cgroup = $cgroup;

        return $this;
    }

    /**
     * Get cgroup
     *
     * @return \Chill\GroupBundle\Entity\CGroup
     */
    public function getCgroup()
    {
        return $this->cgroup;
    }
    
    /**
     * Check that the Role attributed to the current membership is contained
     * within the role of the group's type. 
     * 
     * @param ExecutionContextInterface $context
     */
    public function validRoleForGroup(ExecutionContextInterface $context)
    {
        if (! $this->getCgroup()->getType()->getRoles()->contains($this->getRole())) {
            $context->buildViolation('This role is not allowed by the group type')
                    ->atPath('role')
                    ->addViolation();
        }
    }
}
