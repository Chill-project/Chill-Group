<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Chill\MainBundle\Templating\Events\DelegatedBlockRenderingEvent;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Chill\GroupBundle\Form\Type\CGroupType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * This event add a form "add the group to the event". 
 * 
 * It is called by the ChillEventBundle.
 * 
 * The form is a CGroupType, which allow to choose for the event.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TemplatingEventPostFooterShow implements EventSubscriberInterface
{
    /**
     *
     * @var EngineInterface
     */
    protected $templating;
    
    /**
     *
     * @var FormFactoryInterface
     */
    protected $formFactory;
    
    /**
     *
     * @var Router
     */
    protected $router;
    
    public function __construct(
            EngineInterface $templating,
            FormFactoryInterface $factory,
            Router $router
            )
    {
        $this->templating = $templating;
        $this->formFactory = $factory;
        $this->router = $router;
    }
    
    public static function getSubscribedEvents()
    {
        return array(
            'chill_block.block_footer_show' => array(
                array('addForm', 10)
            )
        );
    }
    
    public function addForm(DelegatedBlockRenderingEvent $e)
    {
        /* @var $event Chill\EventBundle\Entity\Event */
        $event = $e['event'];
        $form = $this->formFactory->createNamed(
                null, 
                FormType::class, 
                array(
                    'event_id' => $event->getId(),
                ),
                array(
                    'method' => 'GET',
                    'csrf_protection' => false,
                    'action' => $this->router
                        ->generate('chill_group_new_group_participation_to_event')
                ));
        
        $form->add('cgroup_id', CGroupType::class, array(
            'center' => $event->getCenter(),
            'placeholder' => 'Pick a group'
        ));
        
        $form->add('event_id', HiddenType::class);
        
        $form->add('submit', SubmitType::class, array(
            'label' => 'Add group to this event'
        ));
        
        $e->addContent($this->templating->render(
                'ChillGroupBundle:Delegated:event_post_footer.html.twig',
                array(
                    'form' => $form->createView()
                )
            ));
    }

}
