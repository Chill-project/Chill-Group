<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Events;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Chill\MainBundle\Templating\Events\DelegatedBlockRenderingEvent;
use Symfony\Component\Templating\EngineInterface;
use Doctrine\ORM\EntityRepository;

/**
 * This class show a short list for vertical menu below the vertical menu
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TemplatingPostVerticalMenuEventSubscriber implements EventSubscriberInterface
{
    /**
     *
     * @var EngineInterface
     */
    protected $templating;
    
    /**
     *
     * @var EntityRepository
     */
    protected $membershipRepository;
    
    public function __construct(
            EngineInterface $templating, 
            EntityRepository $membershipRepository)
    {
        $this->templating = $templating;
        $this->membershipRepository = $membershipRepository;
    }
    
    public static function getSubscribedEvents()
    {
        return array('chill_block.person_post_vertical_menu' => array(
            array('processRendering', 0)
        ));
    }
    
    public function processRendering(DelegatedBlockRenderingEvent $event) 
    {
        $memberships = $memberships = $this->membershipRepository
                ->findBy(array('person' => $event['person']));
        
        $event->addContent(
                $this->templating
                    ->render('ChillGroupBundle:Membership:short_listing.html.twig', array(
                        'memberships' => $memberships,
                        'short_memberships' => $this->createShortMembership($memberships, $event['person']),
                    ))
            );
    }
    
    protected function createShortMembership($memberships, $person)
    {
        $shortMemberships = array();
        
        foreach ($memberships as $membership) {
            $members = array();
            foreach ($membership->getCGroup()->getMembers() as $member) {
                if ($member->getPerson()->getId() !== $person->getId()) {
                    $members[] = $member->getPerson();
                }
            }
            $shortMemberships[$membership->getId()] = $members;
        }
        
        return $shortMemberships;
    }

}
