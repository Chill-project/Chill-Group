<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\GroupBundle\Entity\Type;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;

/**
 * A type to select between active type
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class TypeType extends AbstractType
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $helper) 
    {
        $this->translatableStringHelper = $helper;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        
        // create a local copy for use in Closure
        $helper = $this->translatableStringHelper;
        
        $resolver
                ->setDefaults(array(
                    'class' => Type::class,
                    'query_builder' => function(EntityRepository $er) {
                        $qb = $er->createQueryBuilder('g');
                        return $qb->where($qb->expr()->eq('g.active', ':active'))
                                ->setParameter('active', 't');
                    },
                    'choice_label' => function(Type $t) use ($helper) {
                        return $helper->localize($t->getName());
                    },
                    'choice_attr' => function(Type $t) {
                        return array(
                            'data-link-category' => $t->getId()
                        );
                    },
                    'attr' => array('class' => 'select2 chill-category-link-parent')
                ));
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
    
}
