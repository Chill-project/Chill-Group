<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\GroupBundle\Entity\CGroup;
use Chill\MainBundle\Entity\Center;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\PersonBundle\Entity\Person;

/**
 * Form type to pick a CGroup
 * 
 * Available options : 
 * 
 * - `center` restrict to groups associated with a center, default `null` ;
 * - `person` restrict to center associated with the person **AND** exclude 
 * groups associated with the person (this behaviour can be modified with 
 * `allow_group`). Default `null`
 * - `allow_group` : force the defined group to be present in the select (only if
 * `person` is defined)
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class CGroupType extends AbstractType
{
    /**
     *
     * @var EntityRepository
     */
    protected $groupRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
            EntityRepository $groupRepository,
            TranslatableStringHelper $helper
            )
    {
        $this->groupRepository = $groupRepository;
        $this->translatableStringHelper = $helper;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $options['query_builder'];
        
        // restrict the choices to the given center only
        if ($options['center'] !== NULL and $options['person'] === NULL) {
            /* @var $qb \Doctrine\ORM\QueryBuilder */           
            $qb->andWhere($qb->expr()->eq('g.center', ':center'))
                    ->setParameter('center', $options['center']);
        }
        
        if ($options['person'] !== NULL) {
            $qb->andWhere($qb->expr()->eq('g.center', ':center'))
                    ->setParameter('center', $options['person']->getCenter());
            
            $existingGroups = $this->groupRepository
                    ->findGroupsAttachedTo($options['person']);
            
            // remove the allowed group if exists
            if ($options['allow_group'] instanceof CGroup) {
                $key = array_search($options['allow_group'], $existingGroups);
                unset($existingGroups[$key]);
            } 
            
            if (count($existingGroups) > 0) {
                $qb->andWhere($qb->expr()->notIn('g', ':existingGroups'))
                        ->setParameter('existingGroups', $existingGroups);                
            }

        }
    }
    
    
    public function configureOptions(OptionsResolver $resolver)
    {
        // create local copy for Closure
        $helper = $this->translatableStringHelper;
        
        $resolver
                ->setDefined('center')
                ->setAllowedTypes('center', array(Center::class, 'null'))
                ->setDefault('center', null)
                ->setDefined('person')
                ->addAllowedTypes('person', array(Person::class, 'null'))
                ->setDefault('person', null)
                ->setDefined('allow_group')
                ->setAllowedTypes('allow_group', array(CGroup::class, 'null'))
                ->setDefault('allow_group', null)
                ;
        
        // create a default query builder
        $qb = $this->groupRepository->createQueryBuilder('g');
        $qb->where($qb->expr()->eq('g.active', 'true'));
        
        $resolver
                ->setDefaults(array(
                    'class' => CGroup::class,
                    'query_builder' => $qb,
                    'choice_label' => function(CGroup $g)  {
                        return $g->getName();
                    },
                    'placeholder' => '',
                    'choice_attr' => function(CGroup $g) {
                        return array(
                            'data-link-category' => $g->getType()->getId(),
                            'data-center' => $g->getCenter()->getId()
                        );
                    },
                    'group_by' => function(CGroup $g) use ($helper) {
                        return $helper->localize($g->getType()->getName());
                    }
                ));
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
    
}
