<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Chill\GroupBundle\Entity\CGroup;
use Chill\GroupBundle\Entity\Type;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\GroupBundle\Entity\Role;
use Chill\GroupBundle\Entity\Membership;
use Chill\MainBundle\Entity\Center;

/**
 * Form type to create a membership
 * 
 * available options : 
 * 
 * - `status` (default 'new', might be 'new' or 'update') if the form action will create a new membership (= 'new')
 * or update and existing one ('update')
 * - `center`: the center of the groups 
 * - `person`: the person associated to the membership. This is used to removed unused center
 * 
 * If the status is 'new', the form allow only roles which are associated with the
 * given group's type. 
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class MembershipType extends AbstractType
{
    /**
     *
     * @var EntityRepository
     */
    protected $roleRepository;
    
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(
            EntityRepository $roleRepository,
            \Chill\MainBundle\Templating\TranslatableStringHelper $helper
            )
    {
        $this->roleRepository = $roleRepository;
        $this->translatableStringHelper = $helper;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($options['status'] === 'new') {
            $builder
                ->add('cgroup', \Chill\GroupBundle\Form\Type\CGroupType::class,
                    array(
                        'center' => $options['center'],
                        'label' => 'Group',
                        'attr' => array('class' => 'select2 chill-category-link-parent'),
                        'person' => $options['person']
                    ))
                ;
        }
        
        $builder->addEventListener(
                FormEvents::PRE_SET_DATA, 
                function(FormEvent $event) use ($options) {
                    /* @var $formModifier \Chill\GroupBundle\Entity\Membership */
                    $membership = $event->getData();

                    self::formModifier(
                            $event->getForm(), 
                            $this->roleRepository,
                            $this->translatableStringHelper,
                            $options,
                            $membership->getCGroup() === NULL ? 
                                null : $membership->getCGroup()->getType());
        });
        
        if ($options['status'] === 'new') {
            $builder->get('cgroup')->addEventListener(
                    FormEvents::POST_SUBMIT, 
                    function(FormEvent $event) use ($options) {
                        /* @var $formModifier \Chill\GroupBundle\Entity\CGroup */
                        $group = $event->getForm()->getData();

                        self::formModifier(
                                $event->getForm()->getParent(), 
                                $this->roleRepository,
                                $this->translatableStringHelper,
                                $options,
                                $group->getType());
            }); 
        }
        
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Membership::class)
                ->setDefined('center')
                ->setAllowedTypes('center', array(Center::class, 'null'))
                ->setDefault('center', null)
                ->setDefined('person')
                ->setAllowedTypes('person', array(\Chill\PersonBundle\Entity\Person::class, 'null'))
                ->setDefault('person', null)
                ->setDefined('status')
                ->setAllowedValues('status', array('edit', 'new'))
                ->setDefault('status', 'new')
                ;
        
    }
    
    /**
     * modify the form according to the data associated with the form
     * 
     * @param FormInterface $form
     * @param EntityRepository $roleRepository
     * @param TranslatableStringHelper $helper
     * @param array $options the options of the form
     * @param Type $type (default null) the type of the role to restrict, if required
     * @param type $allowTypeSwitching (default false) if we want to allow other roles than for the given type
     * @param CGroup $allowGroup allow a given group
     */
    public static function formModifier(
            FormInterface $form, 
            EntityRepository $roleRepository, 
            TranslatableStringHelper $helper, 
            $options,
            Type $type = null) 
    {
        $roles = $type === NULL ? 
                $roleRepository->findBy(array('active' => true)) :
                $roleRepository->findBy(array('active' => true, 'type' => $type))
                ;

        $form->add('role', EntityType::class, array(
            'class' => \Chill\GroupBundle\Entity\Role::class,
            'placeholder' => '',
            'choices' => $roles,
            'choice_label' => function(Role $role) use ($helper) {
                return $helper->localize($role->getName());
            },
            'choice_attr' => function(Role $r) {
                return array(
                    'data-link-category' => $r->getType()->getId()
                );
            },
            'attr' => array('class' => 'chill-category-link-child')
        ));
    }
    
}
