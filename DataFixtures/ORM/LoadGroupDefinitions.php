<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\GroupBundle\Entity\Type;
use Chill\GroupBundle\Entity\Role;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadGroupDefinitions extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 20001;
    }

    public function load(ObjectManager $manager)
    {
        echo "Loading group definition\n";
        
        $this->createFamilyType($manager);
        $this->createActivityType($manager);
        
        $manager->flush();
    }
    
    protected function createFamilyType(ObjectManager $manager)
    {
        $type = new Type();
        $type->setName(
                array(
                    'fr' => "Famille", 
                    'nl' => "Familië",
                    'en' => "Family"
                )
            )
            ->setActive(true);
        $manager->persist($type);
        $this->setReference('group_type_family', $type);
        
        $role = new Role();
        $role->setName(array(
                    'fr' => "Parent", 'nl' => 'Ouders', 'en' => 'Parent'
                ))
                ->setActive(true)
                ->setType($type);
        $manager->persist($role);
        $type->addRole($role);
        $this->setReference('group_type_family_parent', $role);
        
        $roleChildren = new Role();
        $roleChildren->setName(array(
                    'fr' => "Enfant", 'nl' => 'Kind', 'en' => 'Child'
                ))
                ->setActive(true)
                ->setType($type);
        $manager->persist($roleChildren);
        $type->addRole($roleChildren);
        $this->setReference('group_type_family_child', $roleChildren);
    }
    
    protected function createActivityType(ObjectManager $manager)
    {
        $type = new Type();
        $type->setName(
                array(
                    'fr' => "Échange de service", 
                    'nl' => "Dienst wisseling",
                    'en' => "Exchange of services"
                )
            )
            ->setActive(true);
        $manager->persist($type);
        $this->setReference('group_type_activity', $type);
        
        $role = new Role();
        $role->setName(array(
                    'fr' => "Participant", 'nl' => 'Deelnemers', 'en' => 'Participant'
                ))
                ->setActive(true)
                ->setType($type);
        $manager->persist($role);
        $type->addRole($role);
        $this->setReference('group_type_activity_participant', $role);
        
        $roleLeader = new Role();
        $roleLeader->setName(array(
                    'fr' => "Meneur", 'nl' => 'Leader', 'en' => 'Leader'
                ))
                ->setActive(false) // the role is disabled !!!!
                ->setType($type);
        $manager->persist($roleLeader);
        $type->addRole($roleLeader);
        $this->setReference('group_type_activity_leader', $roleLeader);
    }

}
