<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\GroupBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\GroupBundle\Entity\CGroup;
use Chill\GroupBundle\Entity\Membership;
use Chill\MainBundle\DataFixtures\ORM\LoadCenters;
use Chill\MainBundle\Entity\Center;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class LoadGroupAssociations extends AbstractFixture implements OrderedFixtureInterface
{
    
    /**
     * Groups randomly created
     *
     * @var CGroup[] 
     */
    protected $randomGroups;
    
    /**
     *
     * @var Faker
     */
    protected $faker;
    
    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_FR');
    }
    
    public function getOrder()
    {
        return 20050;
    }

    public function load(ObjectManager $manager)
    {
        foreach (LoadCenters::$refs as $centerRef) {
            $this->randomGroups = array();
            $center = $this->getReference($centerRef);
            $this->loadRandomGroups($manager, $center);
            $this->loadRandomAssociation($manager, $center);
        }
        
        $manager->flush();
    }
    
    protected function loadRandomGroups(ObjectManager $manager, Center $center)
    {
        $possibleGroupsRef = array('group_type_family', 'group_type_activity', 'group_type_family');
        
        $i = 0;
        do {
            $groupRef = $possibleGroupsRef[array_rand($possibleGroupsRef)];
            switch ($groupRef) {
                case 'group_type_family':
                    $name = 'Family '.$this->faker->lastName;
                    break;
                default:
                    $name = 'Group for '.$this->faker->word;
            }
            
            $g = new CGroup();
            $g
                    ->setType($this->getReference($groupRef))
                    ->setName($name)
                    ->setActive(rand(0, 9) > 1) // set active 80% of the time
                    ->setCenter($center)
                    ;
            $manager->persist($g);
            
            $this->randomGroups[] = $g;
            // increment counter
            $i++;
        } while ($i < 40);
    }
    
    protected function loadRandomAssociation(ObjectManager $manager, Center $center)
    {
        $people = $manager->getRepository('ChillPersonBundle:Person')
                ->findBy(array('center' => $center));
        
        foreach ($people as $person) {
            // add group only on two person on three
            if (rand(0,1) === 1) {
                continue;
            }
            // add between 0 and 4 associations. 
            $max = rand(0,3);
            // do not add the same group twice
            $excludeGroupId = array();
            for ($i=0; $i < $max; $i++) {
                $group = $this->getRandomGroup($excludeGroupId);
                $excludeGroupId[] = $group->getId();
                
                // get a random role
                $roles = $group->getType()->getRoles()->toArray();
                $role = $roles[array_rand($roles)];
                
                $membership = new Membership();
                $membership->setCgroup($group)
                        ->setPerson($person)
                        ->setRole($role);
                $manager->persist($membership);
            }
        }
    }
    
    /**
     * 
     * @param array $excludeIds
     * @return CGroup
     */
    protected function getRandomGroup(array $excludeIds = array())
    {
        $group = $this->randomGroups[array_rand($this->randomGroups)];
        
        if (in_array($group->getId(), $excludeIds)) {
            // we have a duplicate, try again
            return $this->getRandomGroup($excludeIds);
        }
        
        return $group;
    }

}
